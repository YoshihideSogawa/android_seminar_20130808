package com.example.android_seminar_20130808;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.android_seminar_20130808.io.GsonRequest;
import com.example.android_seminar_20130808.io.ImageNoCache;
import com.example.android_seminar_20130808.io.VolleyRequestHolder;
import com.example.android_seminar_20130808.model.SearchResult;

/**
 * 動画をキーワード検索してタイトルとサムネイルを出すサンプルです。
 * 
 * @author yoshihide-sogawa
 * 
 */
public class MainActivity extends FragmentActivity implements Response.Listener<SearchResult>, Response.ErrorListener {

    /** 検索 */
    private static final String URL = "http://gdata.youtube.com/feeds/api/videos?alt=json&q=";

    /** 読み込み中 */
    private View mLoadingView;
    /** キーワード入力 */
    private EditText mKeywordView;
    /** 検索ボタン */
    private View mSearchButton;
    /** タイトル */
    private TextView mTitle;
    /** {@link NetworkImageView} */
    private NetworkImageView mImageView;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 読込中
        mLoadingView = findViewById(R.id.loading);
        // 検索キーワード
        mKeywordView = (EditText) findViewById(R.id.keyword);
        // 検索ボタン
        mSearchButton = findViewById(R.id.search);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String keyword = mKeywordView.getText().toString();
                // 文字が入力されていれば検索する
                if (!TextUtils.isEmpty(keyword)) {
                    try {
                        // スペースとかをエンコードする
                        String url = URL + URLEncoder.encode(keyword, "UTF-8");
                        startSearch(url);
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        // 動画情報
        mTitle = (TextView) findViewById(R.id.title);
        mImageView = (NetworkImageView) findViewById(R.id.image);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onBackPressed() {
        // 読込中ならキャンセル
        if (mLoadingView.getVisibility() == View.VISIBLE) {
            cancelSearch();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * 検索を開始します。
     * 
     * @param url
     *            URL
     */
    private void startSearch(final String url) {
        RequestQueue requestQueue = VolleyRequestHolder.newRequestQueue(this);
        GsonRequest<SearchResult> request = new GsonRequest<SearchResult>(url, SearchResult.class, this, this);
        request.setTag(this);
        requestQueue.add(request);
        mLoadingView.setVisibility(View.VISIBLE);
    }

    /**
     * 検索をキャンセルします。
     */
    private void cancelSearch() {
        VolleyRequestHolder.newRequestQueue(this).cancelAll(this);
        mLoadingView.setVisibility(View.GONE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onResponse(SearchResult response) {
        // 0件結果の場合(TODO:しっかりエラーチェックはしてない)
        if (response.feed.entry == null) {
            Toast.makeText(this, "No result", Toast.LENGTH_LONG).show();
            mLoadingView.setVisibility(View.GONE);
            return;
        }

        // タイトル表示
        mTitle.setText("" + response.feed.entry[0].title.$t);
        // 画像表示
        ImageLoader imageLoader = new ImageLoader(VolleyRequestHolder.newRequestQueue(this), new ImageNoCache());
        mImageView.setImageUrl(response.feed.entry[0].media$group.media$thumbnail[0].url, imageLoader);
        mLoadingView.setVisibility(View.GONE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
        mLoadingView.setVisibility(View.GONE);
    }
}
