package com.example.android_seminar_20130808.io;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Volleyを保持しておくクラスです。
 * 
 * @author yoshihide-sogawa
 */
public final class VolleyRequestHolder {

    /** {@link RequestQueue} */
    private static RequestQueue sRequestQueue;

    /**
     * 非公開のコンストラクタ
     */
    private VolleyRequestHolder() {
        // no instances
    }

    /**
     * {@link RequestQueue}を取得します。
     * 
     * @param context
     *            {@link Context}
     * @return {@link RequestQueue}
     */
    public static RequestQueue newRequestQueue(final Context context) {
        if (sRequestQueue == null) {
            sRequestQueue = Volley.newRequestQueue(context);
        }
        return sRequestQueue;
    }
}
