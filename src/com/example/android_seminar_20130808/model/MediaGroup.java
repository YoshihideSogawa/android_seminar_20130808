package com.example.android_seminar_20130808.model;

public class MediaGroup {
    public MediaCategory[] media$category;
    public MediaCountent[] media$countent;
    public SimpleTypeValue media$description;
    public MediaKeywords media$keywords;
    public MediaPlayer[] mediaPlayer;
    public MediaThumbnail[] media$thumbnail;
    public SimpleTypeValue media$title;
    public YtDuration yt$duration;
    public GdRating gd$rating;
    public YtStatistics yt$statistics;
}
