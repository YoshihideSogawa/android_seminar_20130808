package com.example.android_seminar_20130808.model;

public class MediaThumbnail {
    public String url;
    public int height;
    public int width;
    public String time;
}
