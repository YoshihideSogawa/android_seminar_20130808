package com.example.android_seminar_20130808.model;

public class SearchResult {

    public String version;
    public String encoding;
    public Feed feed;
}
